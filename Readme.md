## Bangbeli Mobile Dev Test Batch 9

buatlah project react native menggunakan expo dengan command berikut ini
```bash
npx create-expo-app -t expo-template-blank-typescript test_bip9
```

pastikan udah terinstall [NodeJs](https://nodejs.org/en/download/current) di os anda\
bisa menggunakan commad berikut untuk mastikannya
```
node -v
```

### Tugas
implementasikan design berikut ini supaya menjadi aplikasi yg utuh menggunakan tech stack berikut
1. React Native (expo)
2. Typescript
3. React Navigation

### Design
https://www.figma.com/file/x89Pc7z55Vw4tsArn4FJCY/bangbeli-test?type=design&mode=design&t=itZFmx9EKXobXtR2-1

### Submission
1. projek yang telah selaesai bisa dimasukkan ke gitlab
2. buat visibility sebagai publik
3. kirim link gitlab ke https://discordapp.com/users/547472440863883305
4. batas pengiriman pada pukul `19:00` tanggal `20-juli-2023`